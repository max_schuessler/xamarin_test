﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Maps;

namespace MapsTest{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SampleMapPage : ContentPage{
		public SampleMapPage (){
			InitializeComponent ();

            var map = new Map(MapSpan.FromCenterAndRadius(
                new Position(41.247223, -96.016319),
                Distance.FromMiles(0.5))) {
                    IsShowingUser = true,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
            var position1 = new Position(41.247223, -96.016319);

            var pin1 = new Pin{
                Type = PinType.Place,
                Position = position1,
                Label = "Google",
                Address = "www.google.com"
            };

            map.Pins.Add(pin1);
		}
	}
}